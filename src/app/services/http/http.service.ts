import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

export interface ApiResponse {
  status: string,
  data: any
}

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  httpError = new Subject<Response>();

  get(path): Observable<any> {
    if (!environment || !environment.apiUrl) {
      throw new Error("http get error: environment variable \"apiUrl\" has not been set!");
    }
    return this.http.get(`${environment.apiUrl}${path}`).pipe(
      tap(
        null,
        error => this.handleError(error)
      )
    );
  }

  handleError(error: Response) {
    this.httpError.next(error);
  }
}
