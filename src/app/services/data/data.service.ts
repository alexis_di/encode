import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HttpService, ApiResponse } from '../http/http.service';

declare const require;
const deck = require('../../../mocks/deck.json');

export interface Card {
  name: string,
  text: string,
  card_type: string,
  type: string,
  family: string,
  atk: string,
  def: string,
  level: number,
  property: string
}

@Injectable({
  providedIn: 'root'
})

export class DataService {

  constructor(private httpService: HttpService) { }

  deck = new Subject<any>();

  loadDeck() {
    this.deck.next(deck);
  }

  cardDetails = new Subject<Card>();
  loadCardDetails(cardName: string) {
    if (!cardName || typeof cardName !== 'string') {
      throw new Error(`getCard error: cardName is required and must be a string.`);
    }
    this.httpService.get(`/card_data/${cardName}`).subscribe((res: ApiResponse) => {
      this.cardDetails.next(res.data);
    }, (error: Response) => {
      // console.log(error.status);
    })
  }

}
