import { Component, OnInit } from '@angular/core';
import { DataService, Card } from './services/data/data.service';

@Component({
  selector: 'encode-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(private dataService: DataService) { }

  _cardSubscription: any;
  cardDetails: Card;

  ngOnInit(): void {
    this._cardSubscription = this.dataService.cardDetails.subscribe((res: Card) => {
      this.cardDetails = res;
    })
  }

  ngOnDestroy(): void {
    this._cardSubscription.unsubscribe();
  }

  loadCardDetails(cardName: string) {
    this.dataService.loadCardDetails(cardName);
  }

}
