import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'encode-image',
  templateUrl: './image.component.html',
  host: {
    class: "encode-image-container"
  },
  styleUrls: ['./image.component.scss'],
})
export class ImageComponent implements OnInit {
  @Input() src: string;
  @Input() width: string | number;
  @Input() height: string | number;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.src.previousValue !== changes.src.currentValue) {
      this.loading = true;
    }
  }

  loading: boolean = true;

  imgLoad(e) {
    if (!this.width) {
      this.width = e.path[0].naturalWidth;
    }
    if (!this.height) {
      this.height = e.path[0].naturalHeight;
    }
    this.loading = false;
  }

}
