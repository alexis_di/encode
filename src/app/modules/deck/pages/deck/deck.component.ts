import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/services/data/data.service';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../../../../services/data/data.service';
import { environment } from '../../../../../environments/environment';

@Component({
  selector: 'encode-deck',
  templateUrl: './deck.component.html',
  styleUrls: ['./deck.component.scss']
})
export class DeckComponent implements OnInit {

  constructor(private route: ActivatedRoute, private dataService: DataService) {
    this.apiUrl = environment.apiUrl;
  }

  fetching: boolean;
  activeCard: string;
  cardDetails: Card;
  _routeSub: any;
  _cardDetailsSub: any;
  apiUrl: string;

  ngOnInit() {
    this.subscribe();
  }

  ngOnDestroy(): void {
    this._routeSub.unsubscribe();
    this._cardDetailsSub && this._cardDetailsSub.unsubscribe();
  }

  subscribe() {
    this._routeSub = this.route.queryParams.subscribe((res) => {
      if (res.card) {
        this.fetching = true;
        this.activeCard = res.card;
        this.dataService.loadCardDetails(res.card);
      }
    });
    this._cardDetailsSub = this.dataService.cardDetails.subscribe((res: Card) => {
      this.fetching = false;
      this.cardDetails = res;
    })
  }

}
