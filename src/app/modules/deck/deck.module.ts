import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { DeckRoutingModule } from './deck-routing.module';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { DeckComponent } from './pages/deck/deck.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DeckRoutingModule,
    SharedModule
  ],
  declarations: [
    SidebarComponent,
    DeckComponent
  ],
  bootstrap: []
})

export class DeckModule { }
