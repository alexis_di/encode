import { Component, OnInit, Input } from '@angular/core';
import { DataService, Card } from '../../../../services/data/data.service';

@Component({
  selector: 'deck-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(private dataService: DataService) { }

  @Input() activeCard: string;

  _deckSubscription: any;
  deck: Array<Card>;

  ngOnInit() {
    this.subscribeToDeck();
  }

  ngOnDestroy(): void {
    this._deckSubscription.unsubscribe();
  }

  subscribeToDeck() {
    this._deckSubscription = this.dataService.deck.subscribe((res) => {
      this.deck = res;
    });
    this.dataService.loadDeck();
  }

}
