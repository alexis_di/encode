import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeckComponent } from './pages/deck/deck.component';
const routes: Routes = [
  {
    path: 'deck',
    component: DeckComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  bootstrap: []
})

export class DeckRoutingModule { }
