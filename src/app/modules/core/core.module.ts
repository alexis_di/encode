import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorPopupComponent } from './components/error-popup/error-popup.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ErrorPopupComponent
  ],
  exports: [
    ErrorPopupComponent
  ]
})

export class CoreModule { }
