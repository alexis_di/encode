import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../../services/http/http.service';

@Component({
  selector: 'encode-error-popup',
  templateUrl: './error-popup.component.html',
  styleUrls: ['./error-popup.component.scss']
})
export class ErrorPopupComponent implements OnInit {

  constructor(private httpService: HttpService) { }

  _errorSubscription: any;
  _hideTimeout: any;
  httpError: Response;

  ngOnInit() {
    this.subscribeToError();
  }

  ngOnDestroy(): void {
    this._errorSubscription.unsubscribe();
  }

  subscribeToError() {
    this._errorSubscription = this.httpService.httpError.subscribe((res: Response) => {
      this.httpError = res;

      // Clear error after 2 seconds (if no other http error occured)
      clearTimeout(this._hideTimeout);
      this._hideTimeout = setTimeout(() => {
        this.httpError = null;
      }, 2000)
    })
  }

}
